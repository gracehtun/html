/* カスタマイズ用Javascript */
// toppage scroll
$('.scroll').on('click', function(){
    $('html,body').animate({
    scrollTop: $('#what_new').offset().top},'slow');
});
// end of toppage scroll


// image height for slick slider
$('.main_slick').on('setPosition', function () {
	jbResizeSlider();
});
 
$(window).on('resize', function(e) {
	jbResizeSlider();
});
 
function jbResizeSlider(){
	$slickSlider = $('.main_slick');
	$slickSlider.find('.slick-slide').height('auto');
 
	var slickTrack = $slickSlider.find('.slick-slide');
	var slickTrackWidth = $(slickTrack).width();
 
	$slickSlider.find('.slide_image').css('height', slickTrackWidth + 'px');
}
// end of image for slick slider